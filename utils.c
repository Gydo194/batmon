#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "utils.h"

char * const read_file(const char * const path)
{
	char *buffer;
	FILE *fd;

	buffer = malloc(READ_FILE_BUFFER_SIZE);
	if(0x00 == buffer) return 0x00;
	bzero(buffer, READ_FILE_BUFFER_SIZE);

	fd = fopen(path, "r");
	if(0x00 == fd) return 0x00;

	fgets(buffer, READ_FILE_BUFFER_SIZE - 1, fd);

	fclose(fd);

	return buffer;
}
