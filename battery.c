#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "battery.h"
#include "utils.h"

u8_t read_battery()
{
	u8_t battery;
	int battery_buffer;
	char * buffer;

	buffer = read_file(BATTERY_READOUT_PATH);
	assert(0x00 != buffer);

	sscanf(buffer, "%d", &battery_buffer);
	free(buffer);
	battery = battery_buffer & 0xFF;

	return battery;
}
