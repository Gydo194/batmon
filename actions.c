#include <stdio.h>
#include <libnotify/notify.h>

#include "actions.h"

void low_action()
{
	notify_init("Battery Monitor");
	NotifyNotification * low_notification = notify_notification_new("Battery Monitor", "Battery is running low", "dialog-information");
	notify_notification_set_urgency(low_notification, NOTIFY_URGENCY_NORMAL);
	notify_notification_show(low_notification, 0);
	g_object_unref(G_OBJECT(low_notification));
	notify_uninit();
}

void critical_action()
{
	notify_init("Battery Monitor");
	NotifyNotification * low_notification = notify_notification_new("Battery Monitor", "Battery is running critically low!", "dialog-information");
	notify_notification_set_urgency(low_notification, NOTIFY_URGENCY_CRITICAL);
	notify_notification_show(low_notification, 0);
	g_object_unref(G_OBJECT(low_notification));
	notify_uninit();
}
