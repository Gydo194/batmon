# batmon

A super small low battery warning daemon

## Installing

* Clone the repository
* execute `sudo make install`

## Running
* Execute `batmond`

The binary is stored at `/usr/bin/batmond` by the `install` make target.

Optionally, you can execute `batmond & disown` to disown the process from your shell,
so it will continue running in the background when your shell exits.

## Configuration
Batmon is written with the "suckless" philosophy in mind, meaning all configuration is done in the
program's source code.

* The battery readout file is defined in `battery.h`.
* The check delay is defined in `config.h`.
