#include "thresholds.h"
#include "battery.h"
#include "states.h"
#include "utils.h"

void check()
{
	u8_t battery;

	battery = read_battery();

	if(IN_RANGE(battery, 100, THRESHOLD_LOW))
	{
		transition_normal();
		return;
	}


	if(IN_RANGE(battery, THRESHOLD_LOW, THRESHOLD_CRITICAL))
	{
		transition_low();
		return;
	}

	if(IN_RANGE(battery, THRESHOLD_CRITICAL, 0))
	{
		transition_critical();
		return;
	}
}
