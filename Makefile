CC = gcc

CFLAGS = -g
CFLAGS_OBJ = $(CFLAGS) -c -lnotify
CFLAGS_BUILD = $(CFLAGS) -o main
CFLAGS_TEST = $(CFLAGS) -o test
CFLAGS_LIBNOTIFY=`pkg-config --cflags --libs libnotify`

run: build
	./main

test: test_build
	./test

build: main.o states.o actions.o utils.o battery.o thresholds.o
	$(CC) $(CFLAGS_BUILD) $(CFLAGS_LIBNOTIFY) main.o states.o actions.o utils.o battery.o thresholds.o

test_build: test.o states.o actions.o utils.o battery.o thresholds.o
	$(CC) $(CFLAGS_TEST) $(CFLAGS_LIBNOTIFY) test.o states.o actions.o utils.o battery.o thresholds.o

states.o:
	$(CC) $(CFLAGS_OBJ) states.c

actions.o:
	$(CC) $(CFLAGS_OBJ) $(CFLAGS_LIBNOTIFY) actions.c

utils.o:
	$(CC) $(CFLAGS_OBJ) utils.c

battery.o:
	$(CC) $(CFLAGS_OBJ) battery.c

thresholds.o:
	$(CC) $(CFLAGS_OBJ) thresholds.c

install: build
	install -s -m 0755 ./main /usr/bin/batmond

clean:
	rm -f main test *.o
