#include "states.h"
#include "battery.h"
#include "utils.h"
#include "thresholds.h"

int main(void)
{
	transition_low();
	transition_critical();

	transition_low();
	transition_critical();

	transition_low();
	transition_low();
	transition_critical();
	transition_critical();

	transition_low();
	transition_low();

	transition_normal();
	transition_critical();
	transition_low();
	
	transition_normal();
	transition_low();
	transition_critical();

	char * const filecontent = read_file("/tmp/test");
	printf("Read: '%s'\n", filecontent);
	free(filecontent);

	u8_t battery = read_battery();
	printf("Battery: '%d'\n", battery);

	check();
}
