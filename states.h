#ifndef STATES_H
#define STATES_H

#define STATE_NORMAL	0
#define STATE_LOW	1
#define STATE_CRITICAL	2

void transition_normal();
void transition_low();
void transition_critical();

#endif
