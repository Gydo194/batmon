#include "types.h"
#include "states.h"
#include "actions.h"

static u8_t current_state;

void transition_normal()
{
	current_state = STATE_NORMAL;
}

void transition_low()
{
	if(STATE_NORMAL == current_state)
	{
		low_action();
	}

	current_state = STATE_LOW;
}

void transition_critical()
{
	if(STATE_CRITICAL != current_state)
	{
		critical_action();
	}
	
	current_state = STATE_CRITICAL;
}
