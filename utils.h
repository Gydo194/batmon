#ifndef UTILS_H
#define UTILS_H

#define IN_RANGE(num, hi, lo) (num <= hi && num >= lo)

#define READ_FILE_BUFFER_SIZE	128

char * const read_file(const char * const path);

#endif
