#ifndef BATTERY_H
#define BATTERY_H

#include "types.h"

#define BATTERY_READOUT_PATH "/sys/devices/LNXSYSTM:00/LNXSYBUS:00/PNP0A08:00/device:19/PNP0C09:00/PNP0C0A:00/power_supply/BAT0/capacity"

u8_t read_battery();

#endif
